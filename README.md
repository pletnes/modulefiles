# Modulefiles #

The [environment modules](http://modules.sourceforge.net/) package is widely used to handle multiple installations of the same software package on the same machine. This repository is a collection of module files for common software. You may need to adapt the files to apply them to your system; however, I have attempted to write the files so that only the version number and installation prefix have to be changed.

## Feedback

Any feedback is welcome, in particular, if you've found a bug. Note, however, that the conventions of your machine may differ from the ones assumed in these modules; in particular, installation location might differ.